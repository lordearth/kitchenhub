'use strict'
const View = use('View')

class ContactController {
  index ({ request, response }) {
    return response.send(View.render('contact'))
  }
}

module.exports = ContactController
