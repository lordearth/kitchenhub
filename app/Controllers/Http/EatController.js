'use strict'
const View = use('View')

class EatController {
  index ({ request, response }) {
    return response.send(View.render('eat'))
  }
}

module.exports = EatController
