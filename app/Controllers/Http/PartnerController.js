'use strict'
const View = use('View')

class PartnerController {
  index ({ request, response }) {
    return response.send(View.render('partner'))
  }
}

module.exports = PartnerController
