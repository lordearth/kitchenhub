'use strict'
const View = use('View')

class HomeController {
  index ({ request, response }) {
    return response.send(View.render('home'))
  }

}

module.exports = HomeController
