'use strict'

/*
|--------------------------------------------------------------------------
| Routes
|--------------------------------------------------------------------------
|
| Http routes are entry points to your web application. You can create
| routes for different URL's and bind Controller actions to them.
|
| A complete guide on routing is available here.
| http://adonisjs.com/docs/4.1/routing
|
*/

/** @type {typeof import('@adonisjs/framework/src/Route/Manager')} */
const Route = use('Route')

Route.any('/', 'HomeController.index').as('home')

Route.any('eat', 'EatController.index').as('eat')

Route.any('partner', 'PartnerController.index').as('partner')
Route.post('partner', 'PartnerController.save').as('partner.save')

Route.any('contact', 'ContactController.index').as('contact')
Route.post('contact', 'ContactController.save').as('contact.save')
